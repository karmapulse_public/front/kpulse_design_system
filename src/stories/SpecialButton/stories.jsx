import React from 'react';

import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';

import OrderButton from '../../lib/OrderButton';
import README from './README.md';


storiesOf('Special Button', module)
    .add(
        'Order',
        () => (
            <div>
                <OrderButton
                    round
                    label="Fecha"
                    bgColor="#576d88"
                    fontColor="#ffffff"
                />
            </div>
        )
    );
