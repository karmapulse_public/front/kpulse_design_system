import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faComments, faChevronRight, faStreetView, faArchive } from '@fortawesome/free-solid-svg-icons';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, color } from '@storybook/addon-knobs';


import Modal from '../../lib/Modal';
import Button from '../../lib/Button';

import README from './README.md';

storiesOf('Modal', module)
    .addDecorator(withKnobs)
    .add(
        'default',
        () => {
            return (
                <div style={{ height: '80vh' }}>
                    <Modal
                        title="Título Demo"
                        trigger={event => <Button bgColor="#58af6e" onClick={event}>Modal</Button>}
                    />
                </div>
            );
        }, { info: README }
    )
    .add(
        'Modal with footer',
        () => {
            return (
                <div style={{ height: '80vh' }}>
                    <Modal
                        title="Título Demo"
                        trigger={event => <Button bgColor="#58af6e" onClick={event}>Modal</Button>}
                        actions={[
                            <Button bgColor="#58af6e" secondary onClick={action('click-accept')}>Aceptar</Button>,
                            <Button bgColor="#58af6e" onClick={action('click-cancel')}>Cancelar</Button>
                        ]}
                    />
                </div>
            );
        }
    )
    .add(
        'Modal controler',
        () => {
            return (
                <div style={{ height: '80vh' }}>
                    <Modal
                        title="Título Demo"
                        trigger={event => <Button bgColor="#58af6e" onClick={event}>Modal</Button>}
                        closeClick={false}
                        controlButtom={true}
                        actions={[
                            <Button bgColor="#58af6e" secondary onClick={action('click-accept')}>Aceptar</Button>,
                            <Button bgColor="#58af6e" onClick={action('click-cancel')}>Cancelar</Button>
                        ]}
                    />
                </div>
            );
        }
    );
