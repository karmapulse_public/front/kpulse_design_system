import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text, color } from '@storybook/addon-knobs';

import Search from '../../lib/Search/Search';

import README from './README.md';

storiesOf('Search', module)
    .addDecorator(withKnobs)
    .add(
        'Busqueda',
        () => {
            const bgContainer = color('Background color', '#1b314a');
            const placeholder = text('Placeholder', 'Search');
            const fontColor = color('color', '#fff');
            const borderFocus = color('border', '#58af6e');
            const colorIcon = color('Icon', '#fff');

            return (
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        background: bgContainer,
                        width: '300px',
                        height: '300px',
                    }}
                >
                    <Search
                        initValue=""
                        fontColor={fontColor}
                        placeholder={placeholder}
                        borderFocus={borderFocus}
                        colorIcon={colorIcon}
                        // onChange={algo => console.log(algo)}
                    />
                </div>
            );
        }, { info: README }
    );
