import React from 'react';

import { storiesOf } from '@storybook/react';

import { action } from '@storybook/addon-actions';
import CheckboxList from '../../lib/CheckboxList';

import README from './README.md';

const items = [
    {
        id: 'aguascalientes',
        key: 'Aguascalientes',
    },
    {
        id: 'baja california',
        key: 'Baja California',
    },
    {
        id: 'baja california sur',
        key: 'Baja California Sur',
    },
    {
        id: 'campeche',
        key: 'Campeche',
    },
    {
        id: 'chiapas',
        key: 'Chiapas',
    },
    {
        id: 'ciudad de mexico',
        key: 'Ciudad de México',
    },
    {
        id: 'chihuahua',
        key: 'Chihuahua',
    },
    {
        id: 'coahuila',
        key: 'Coahuila',
    },
    {
        id: 'colima',
        key: 'Colima',
    },
    {
        id: 'durango',
        key: 'Durango',
    },
    {
        id: 'mexico',
        key: 'Estado de México',
    },
    {
        id: 'guanajuato',
        key: 'Guanajuato',
    },
    {
        id: 'guerrero',
        key: 'Guerrero',
    },
    {
        id: 'hidalgo',
        key: 'Hidalgo',
    },
    {
        id: 'jalisco',
        key: 'Jalisco',
    },
    {
        id: 'michoacan',
        key: 'Michoacán',
    },
    {
        id: 'morelos',
        key: 'Morelos',
    },
    {
        id: 'nayarit',
        key: 'Nayarit',
    },
    {
        id: 'nuevo leon',
        key: 'Nuevo León',
    },
    {
        id: 'oaxaca',
        key: 'Oaxaca',
    },
    {
        id: 'puebla',
        key: 'Puebla',
    },
    {
        id: 'queretaro',
        key: 'Querétaro',
    },
    {
        id: 'quintana roo',
        key: 'Quintana Roo',
    },
    {
        id: 'san luis potosi',
        key: 'San Luis Potosí',
    },
    {
        id: 'sinaloa',
        key: 'Sinaloa',
    },
    {
        id: 'sonora',
        key: 'Sonora',
    },
    {
        id: 'tabasco',
        key: 'Tabasco',
    },
    {
        id: 'tamaulipas',
        key: 'Tamaulipas',
    },
    {
        id: 'tlaxcala',
        key: 'Tlaxcala',
    },
    {
        id: 'veracruz',
        key: 'Veracruz',
    },
    {
        id: 'yucatan',
        key: 'Yucatán',
    },
    {
        id: 'zacatecas',
        key: 'Zacatecas',
    },
    {
        id: 'Turnada a la Comisión de Puntos Constitucionales',
        key: 'Turnada a la Comisión de Puntos Constitucionales',
    }
];

const valueItems = [
    {
        id: 'all',
        key: 'Todos los estados del mundo mundial'
    },
];
storiesOf('CheckboxList', module)
    .add(
        'CheckboxList',
        () => {
            const itemSelect = (props) => {
                const { key, id } = props;
                action(`Values ${key}, ${id}`);
            };
            return (
                <div>
                    <CheckboxList
                        listItems={items}
                        listBgColor="#ffffff"
                        itemHoverColor="#b11833"
                        textColor="#000000"
                        arrowColor="#58af6e"
                        onClick={itemSelect}
                        defaultItem="all"
                        valueAllItem={valueItems}
                        value="key"
                        enableTooltip
                    />
                </div>
            );
        }
    );
