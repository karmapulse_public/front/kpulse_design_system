import React from 'react';

import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';

import Toggle from '../../lib/Toggle';

import README from './README.md';

storiesOf('Toggle', module)
    .add(
        'Toogle Default',
        () => (
            <div>
                <Toggle
                    activeColor="#58af6e"
                    disabledColor="#0a1b2f"
                    bgCircle="#1b314a"
                    borderCircle="#394f69"
                    textColor="#0a1b2f"
                >
                    Tweet con imagen
                </Toggle>
            </div>
        )
    );
