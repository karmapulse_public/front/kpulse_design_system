import React from 'react';

import { storiesOf } from '@storybook/react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';

import ButtonMenu from '../../lib/ButtonMenu';

import README from './README.md';

const items = [
    { label: 'Mover a leads', onClick: () => ({}) },
    { label: 'Mover a leads descartados', onClick: () => ({}) }
];

storiesOf('Menu', module)
    .add(
        'From button',
        () => (
            <div>
                <ButtonMenu
                    listItems={items}
                    listBgColor="#2d578b"
                    itemHoverColor="#58af6e"
                >
                    <FontAwesomeIcon icon={faEllipsisV} />
                </ButtonMenu>
            </div>
        )
    );
