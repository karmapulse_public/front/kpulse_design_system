import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faComments, faChevronRight, faStreetView, faArchive } from '@fortawesome/free-solid-svg-icons';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, color } from '@storybook/addon-knobs';


import NavBar from '../../lib/NavBar';
import NavItem from '../../lib/NavItem';

import README from './README.md';

storiesOf('NavBar', module)
    .addDecorator(withKnobs)
    .add(
        'default',
        () => {
            const name = text('Name', 'new button');
            const bgColorNavBar = color('Background color', '#233851');
            const bgColorNavItem = color('Active color', '#58af6e');
            const active = boolean('Active', true);

            return (
                <div style={{ height: '80vh' }}>
                    <NavBar bgColor={bgColorNavBar}>
                        <NavItem bgColor={bgColorNavItem} active={active} onClick={action('button-click')}>
                            <FontAwesomeIcon icon={faStreetView} />
                            {name}
                            <FontAwesomeIcon icon={faChevronRight} />
                        </NavItem>
                        <NavItem bgColor={bgColorNavItem}>
                            <FontAwesomeIcon icon={faComments} />
                            {name}
                            <FontAwesomeIcon icon={faChevronRight} />
                        </NavItem>
                        <NavItem bgColor={bgColorNavItem}>
                            <FontAwesomeIcon icon={faArchive} />
                            {name}
                            <FontAwesomeIcon icon={faChevronRight} />
                        </NavItem>
                    </NavBar>
                </div>
            );
        }, { info: README }
    )
    .add(
        'expanded',
        () => {
            const name = text('Name', 'new button');
            const bgColorNavBar = color('NavBar bgColor', '#233851');
            const bgColorNavItem = color('NavItem bgColor', '#58af6e');

            return (
                <div style={{ height: '90vh' }}>
                    <NavBar bgColor={bgColorNavBar} closeButton expanded>
                        <NavItem bgColor={bgColorNavItem} active onClick={action('button-click')}>
                            <FontAwesomeIcon icon={faStreetView} />
                            {name}
                            <FontAwesomeIcon icon={faChevronRight} />
                        </NavItem>
                        <NavItem bgColor={bgColorNavItem}>
                            <FontAwesomeIcon icon={faComments} />
                            {name}
                            <FontAwesomeIcon icon={faChevronRight} />
                        </NavItem>
                        <NavItem bgColor={bgColorNavItem}>
                            <FontAwesomeIcon icon={faArchive} />
                            {name}
                            <FontAwesomeIcon icon={faChevronRight} />
                        </NavItem>
                    </NavBar>
                </div>
            );
        }
    );
