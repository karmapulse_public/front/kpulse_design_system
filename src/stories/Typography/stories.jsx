import React from 'react';

import { storiesOf } from '@storybook/react';

import Typography from '../../lib/Typography';

import README from './README.md';

const { Title, Paragraph } = Typography;

storiesOf('Typography', module)
    .add(
        'Titulos',
        () => [
            <Title key={1} level={1} bold styles={{ fontSize: '50px' }} >Titulo</Title>,
            <Title key={2} level={2} light>Titulo</Title>,
            <Title key={3} level={3} color="red">Titulo</Title>,
            <Title key={4} level={4} >Titulo</Title>,
            <Title key={5} level={5} >Titulo</Title>,
            <Title key={6} level={6} >Titulo</Title>
        ]
    )
    .add(
        'Parrafos',
        () => [
            <Paragraph key={1} bold >
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </Paragraph>,
            <Paragraph key={2} small light>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </Paragraph>,
            <Paragraph key={3} large >
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </Paragraph>,
        ]
    );
