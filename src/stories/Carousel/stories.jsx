import React from 'react';

import { storiesOf } from '@storybook/react';

import Carousel from '../../lib/Carousel';

// import README from './README.md';


storiesOf('Carousel', module)
    .add(
        'Carousel Default',
        () => (
            <Carousel>
                <div>Componente 1</div>
                <div>Componente 2</div>
                <div>Componente 3</div>
            </Carousel>
        )
    );
