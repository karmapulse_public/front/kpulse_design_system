import React from 'react';

import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';

import Checkbox from '../../lib/Checkbox';
import README from './README.md';


storiesOf('Checkbox', module)
    .add(
        'Checkbox',
        () => (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', background: '#233851', width: '500px', height: '500px' }}>
                <Checkbox
                    value={0}
                    label="Primer Checkbox"
                    fontColor="#ffffff"
                    bgColor="#58af6e"
                    onChange={e => console.log(e)}
                />
            </div>
        )
    )
    .add(
        'Checkbox Disabled',
        () => (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', background: '#233851', width: '500px', height: '500px' }}>
                <Checkbox
                    value={0}
                    label="Segundo Checkbox"
                    fontColor="#ffffff"
                    bgColor="#58af6e"
                    disabled
                />
            </div>
        )
    );