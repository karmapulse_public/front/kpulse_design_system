import React from 'react';

import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';

import Chip from '../../lib/Chip';
import README from './README.md';


storiesOf('Chip', module)
    .add(
        'Simple Chip',
        () => (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <Chip
                    label="Primer Chip"
                    close={false}
                    fontColor="#ffffff"
                    bgColor="#576d88"
                />
            </div>
        )
    )
    .add(
        'WIth Icon Chip',
        () => (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <Chip
                    label="Prueba Chip"
                    fontColor="#ffffff"
                    bgColor="#576d88"
                    onClose={algo => console.log(algo)}
                />
            </div>
        )
    );
