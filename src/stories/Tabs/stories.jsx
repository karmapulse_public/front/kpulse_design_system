import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressBook } from '@fortawesome/free-solid-svg-icons';

import { storiesOf } from '@storybook/react';

import { Tabs, Tab } from '../../lib/Tabs';

import README from './README.md';

const Component = () => {
    const [value, setValue] = useState();
    return (
        <div>
            <Tabs
                value={value}
                onChange={(i) => { setValue(i); }}
                tabsWidth={100}
            >
                <Tab label="Tab 1" />
                <Tab label="Tab 2" />
                <Tab label="Tab 3" />
                <Tab label="Tab 4" />
                <Tab label="Tab 5" />
            </Tabs>
        </div>
    );
};

storiesOf('Tabs', module)
    .add(
        'Simple',
        () => <Component />
    );

