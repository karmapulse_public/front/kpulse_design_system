import React from 'react';

import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { action } from '@storybook/addon-actions';

import Button from '../../lib/Button';

import README from './README.md';


storiesOf('Button', module)
    .add(
        'Primary',
        () => (
            <div>
                <div style={{ display: 'flex' }}>
                    <Button
                        small
                        bgColor="#58af6e"
                        style={{ margin: 10 }}
                        onClick={action('button-click')}
                    >
                        Small
                    </Button>
                    <Button
                        shape
                        bgColor="#58af6e"
                        style={{ margin: 10 }}
                    >
                        Default
                        <FontAwesomeIcon icon={faArrowUp} />
                    </Button>
                    <Button
                        disabled
                        bgColor="#58af6e"
                        style={{ margin: 10 }}
                    >
                        Disabled
                    </Button>
                    <Button
                        large
                        bgColor="#58af6e"
                        style={{ margin: 10 }}
                    >
                        Large
                    </Button>
                </div>
            </div>
        )
    ).add(
        'Secondary',
        () => (
            <div>
                <div style={{ display: 'flex' }}>
                    <Button
                        small
                        secondary
                        bgColor="#e18c3b"
                        style={{ margin: 10 }}
                    >
                        Small
                    </Button>
                    <Button
                        secondary
                        bgColor="#e18c3b"
                        style={{ margin: 10 }}
                    >
                        Default
                    </Button>
                    <Button
                        disabled
                        secondary
                        bgColor="#e18c3b"
                        style={{ margin: 10 }}
                    >
                        Disabled
                    </Button>
                    <Button
                        large
                        secondary
                        bgColor="#e18c3b"
                        style={{ margin: 10 }}
                    >
                        Large
                    </Button>
                </div>
            </div>
        )
    ).add(
        'Round',
        () => (
            <div>
                <div style={{ display: 'flex' }}>
                    <Button
                        round
                        small
                        bgColor="#e18c3b"
                        style={{ margin: 10 }}
                    >
                        Small
                    </Button>
                    <Button
                        round
                        bgColor="#e18c3b"
                        style={{ margin: 10 }}
                    >
                        Default
                        <FontAwesomeIcon icon={faArrowUp} />
                    </Button>
                    <Button
                        round
                        disabled
                        bgColor="#e18c3b"
                        style={{ margin: 10 }}
                    >
                        Disabled
                    </Button>
                    <Button
                        round
                        large
                        bgColor="#e18c3b"
                        style={{ margin: 10 }}
                    >
                        Large
                    </Button>
                </div>
            </div>
        )
    )
    .add(
        'Ghost',
        () => (
            <div>
                <div style={{ display: 'flex' }}>
                    <Button
                        ghost
                        small
                        bgColor="#58af6e"
                        style={{ margin: 10 }}
                    >
                        Ghost
                    </Button>
                    <Button
                        ghost
                        bgColor="#58af6e"
                        style={{ margin: 10 }}
                    >
                        Ghost
                    </Button>
                    <Button
                        ghost
                        bgColor="#58af6e"
                        style={{ margin: 10 }}
                    >
                        Ghost
                        <FontAwesomeIcon icon={faArrowUp} />
                    </Button>
                </div>
            </div>
        )
    );
