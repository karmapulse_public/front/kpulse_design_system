import tinycolor from 'tinycolor2';

const colorTone = (color, amt) => {
    const col = tinycolor(color);

    if (amt < 0) {
        col.darken(amt * -1);
    } else {
        col.lighten(amt);
    }

    return col.toString('hex');
};

export default colorTone;
