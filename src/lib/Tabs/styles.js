import { css } from 'glamor';

const TabsStyles = (selected, containerWidth, numTabs, tabsWidth, lineColor) => {
    const fixedWidth = tabsWidth < 0 ? (containerWidth / numTabs) : tabsWidth;

    return css({
        '.tabs-container': {
            position: 'relative',
            width: '100%',
            display: 'flex',
            ' > button': {
                width: `${fixedWidth}px`
            }
        },
        '.tabs-container:before': {
            content: '""',
            position: 'absolute',
            bottom: '0px',
            left: `${(selected * fixedWidth)}px`,
            width: fixedWidth,
            height: 5,
            backgroundColor: lineColor,
            transition: 'left 0.5s cubic-bezier(.94,1.89,.7,.69)'
        }
    });
};

const TabStyles = (fontColor, bgColor, small) => css({
    display: 'flex',
    padding: small ? '5px 0px 10px' : '5px 0px 20px',
    border: 'none',
    color: fontColor,
    backgroundColor: bgColor,
    outline: 'none',
    cursor: 'pointer',
    alignItems: 'center',
    justifyContent: 'center',
    ' > svg': {
        marginRight: 10
    }
});

export { TabsStyles, TabStyles };
