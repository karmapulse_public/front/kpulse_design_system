import React, { Children, useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { TabsStyles } from './styles';

const propTypes = {
    value: PropTypes.number,
    onChange: PropTypes.func,
    children: PropTypes.element,
    tabsWidth: PropTypes.number,
    indicatorColor: PropTypes.string,
    fontColor: PropTypes.string,
    bgColor: PropTypes.string,
    small: PropTypes.bool
};

const defaultProps = {
    value: 0,
    onChange: () => ({}),
    children: null,
    tabsWidth: -1,
    indicatorColor: '#58af6e',
    fontColor: '#FFF',
    bgColor: '#2d578b',
    small: false
};

const Tabs = (props) => {
    const {
        children,
        onChange,
        tabsWidth,
        value,
        indicatorColor,
        fontColor,
        bgColor,
        small,
    } = props;
    const tabsBar = useRef();
    const [styles, setStyles] = useState({});

    useEffect(() => {
        setStyles(TabsStyles(
            value,
            tabsBar.current.offsetWidth,
            Children.count(children),
            tabsWidth,
            indicatorColor
        ));
    }, [tabsBar.current, setStyles, value]);

    return (
        <div
            className="tabs-container"
            ref={tabsBar}
            {...styles}
        >
            {
                Children.map(children, (child, index) => (
                    React.cloneElement(child, {
                        label: child.props.label,
                        icon: child.props.icon,
                        fontColor,
                        bgColor,
                        small,
                        isActive: index === value,
                        onChange: () => onChange(index)
                    })
                ))
            }
        </div>
    );
};

Tabs.propTypes = propTypes;
Tabs.defaultProps = defaultProps;

export default Tabs;
