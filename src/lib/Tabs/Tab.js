import React from 'react';
import PropTypes from 'prop-types';

import { TabStyles } from './styles';

const propTypes = {
    label: PropTypes.string,
    icon: PropTypes.func,
    fontColor: PropTypes.string.isRequired,
    bgColor: PropTypes.string.isRequired,
    small: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
    isActive: PropTypes.bool.isRequired
};

const defaultProps = {
    label: '',
    icon: () => null
};

const Tab = (props) => {
    const {
        label,
        onChange,
        icon,
        bgColor,
        fontColor,
        small,
        isActive
    } = props;

    return (
        <button
            {...TabStyles(fontColor, bgColor, small)}
            type="button"
            className={`tab ${isActive ? 'tab--active' : 'tab--inactive'}`}
            onClick={() => onChange()}
        >
            {icon()}
            {label}
        </button>
    );
};

Tab.propTypes = propTypes;
Tab.defaultProps = defaultProps;

export default Tab;
