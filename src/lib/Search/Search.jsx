import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faTimes } from '@fortawesome/free-solid-svg-icons';

import styles from './styles';

const Search = (props) => {
    const [value, setValue] = useState(props.initValue);

    const clean = () => {
        const reset = '';
        props.onChange(reset);
        props.onEnter(reset);
        setValue(reset);
    };

    const symbol = () => {
        if (value !== '') {
            return <FontAwesomeIcon icon={faTimes} onClick={clean} />;
        }
        return null;
    };

    const handleEnter = (event) => {
        if (event.key === 'Enter') {
            props.onEnter(value);
        }
    };

    const handleChange = (e) => {
        const newValue = e.target.value;
        props.onChange(newValue);
        setValue(newValue);
    };

    return (
        <div {...styles(props)} >
            <input
                type="text"
                value={value}
                style={props.style}
                onChange={handleChange}
                onKeyDown={handleEnter}
                colorFocus={props.borderFocus}
                placeholder={props.placeholder}
            />
            <FontAwesomeIcon icon={faSearch} colorIcon={props.colorIcon} />
            {symbol()}
        </div>
    );
};

Search.defaultProps = {
    initValue: '',
    onChange: () => ({}),
    onEnter: () => ({}),
    style: {},
    placeholder: '',
    borderFocus: '#58af6e',
    colorIcon: '#fff'
};

Search.propTypes = {
    initValue: PropTypes.string,
    onChange: PropTypes.func,
    onEnter: PropTypes.func,
    style: PropTypes.shape({}),
    placeholder: PropTypes.string,
    borderFocus: PropTypes.string,
    colorIcon: PropTypes.string
};

export default Search;
