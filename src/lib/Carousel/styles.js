import { css } from 'glamor';
import tinycolor from 'tinycolor2';

const itemsPosition = {
    top: 'flex-end',
    bottom: 'flex-start',
    left: 'center',
    right: 'center'
};

const styles = (width, height, arrowsColorT, dotsColorT, dotsPosition, withDots, arrowsHeight) => {
    const arrowsColor = tinycolor(arrowsColorT);
    const dotsColor = tinycolor(dotsColorT);
    arrowsColor.setAlpha(0.5);
    dotsColor.setAlpha(0.5);
    return (
        css({
            '.carousel': {
                position: 'relative',
                display: 'flex',
                width,
                height,
                alignItems: withDots ? itemsPosition[dotsPosition] : 'center'
            },
            ' .carousel-items': {
                position: 'relative',
                zIndex: 1,
                width: '100%',
                height,
                margin: '0px auto',
                overflow: 'hidden',
                backgroundColor: 'transparent',
                '&__item': {
                    position: 'absolute',
                    top: '50%',
                    width: '100%',
                    transform: 'translate(0, -50%)',
                    transition: 'left 0.5s ease',
                    '&--active': {
                        left: '0%',
                    },
                    '&--inactive-left': {
                        left: '-120%'
                    },
                    '&--inactive-right': {
                        left: '120%'
                    }
                }
            },
            ' .carousel-ctrArrow': {
                position: 'absolute',
                zIndex: 2,
                display: 'flex',
                margin: 0,
                padding: 0,
                backgroundColor: 'transparent',
                border: 'none',
                alignItems: 'center',
                outline: 'none',
                cursor: 'pointer',
                '&--top': {
                    bottom: '100%'
                },
                '&--bottom': {
                    top: '100%'
                },
                '&--side': {
                    top: '50%',
                    transform: 'translate(0%, -50%)',
                },
                '&__left': {
                    right: 'calc(100% + 20px)'
                },
                '&__right': {
                    left: 'calc(100% + 20px)'
                },
                ' > svg': {
                    color: arrowsColor.toString(),
                    transition: 'all 0.25s ease',
                    height: arrowsHeight
                },
                '&:hover > svg': {
                    color: arrowsColorT
                },
                '&--inactive': { display: 'none' },
            },
            ' .carousel-ctrDots': {
                position: 'absolute',
                display: 'flex',
                zIndex: 1,
                '&__dot': {
                    position: 'relative',
                    width: 7,
                    height: 7,
                    padding: 0,
                    border: 'none',
                    borderRadius: '50%',
                    backgroundColor: dotsColor.toString(),
                    outline: 'none',
                    transition: 'all 0.25s ease',
                    cursor: 'pointer',
                    '&--active': {
                        background: dotsColorT,
                    },
                    '&:after': {
                        content: '""',
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        width: 'calc(100% + 10px)',
                        height: 'calc(100% + 10px)',
                        backgroundColor: 'transparent',
                        transform: 'translate(-50%, -50%)'
                    },
                },
                '&--top': {
                    bottom: '100%',
                    marginBottom: '5px'
                },
                '&--bottom': {
                    top: '100%',
                    marginTop: '5px'
                },
                '&--top, &--bottom': {
                    left: '50%',
                    transform: 'translate(-50%, 0)',
                    ' .carousel-ctrDots__dot:not(:last-child)': {
                        marginRight: '10px'
                    },
                },
                '&--left': {
                    left: 0,
                },
                '&--right': {
                    right: 0,
                },
                '&--right, &--left': {
                    top: '50%',
                    transform: 'translate(0, -50%)',
                    flexDirection: 'column',
                    ' .carousel-ctrDots__dot:not(:last-child)': {
                        marginBottom: '10px'
                    },
                },
            },
        })
    );
};

export default styles;
