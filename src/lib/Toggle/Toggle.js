import React from 'react';
import PropTypes from 'prop-types';

import styles from './ToggleStyles';

const Toggle = props => (
    <div
        {...styles(props)}
    >
        <div className="container">
            <label className="switch" htmlFor="checkbox">
                <input type="checkbox" id="checkbox" onClick={props.onClick} />
                <div className="slider round" />
            </label>
        </div>
        <div className="text">
            {props.children}
        </div>
    </div>
);
Toggle.propTypes = {
    onClick: PropTypes.func,
    children: PropTypes.any,
};

Toggle.defaultProps = {
    onClick: () => ({}),
    children: null,
};

export default Toggle;
