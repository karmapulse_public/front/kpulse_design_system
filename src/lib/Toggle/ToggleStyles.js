import { css } from 'glamor';

export default (props) => {
    const bgActive = props.activeColor;
    const bgdisable = props.disabledColor;
    const backgrondCircle = props.bgCircle;
    const bordercircle = props.borderCircle;
    const text = props.textColor;

    return css({
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        ' .switch': {
            display: 'block',
            height: 4,
            position: 'relative',
            width: 48,
            ' input': {
                display: 'none'
            },
            ' .slider': {
                backgroundColor: bgdisable,
                bottom: 0,
                cursor: 'pointer',
                left: 0,
                position: 'absolute',
                right: 0,
                top: 0,
                transition: '.4s',
            },
            ' .slider:before': {
                backgroundColor: backgrondCircle,
                border: `solid 2px ${bordercircle}`,
                bottom: '-12px',
                content: '""',
                height: '24px',
                left: '-5px',
                position: 'absolute',
                transition: '.4s',
                width: '24px',
            },
            ' input:checked + .slider': {
                backgroundColor: bgActive
            },
            ' input:checked + .slider:before': {
                transform: 'translateX(26px)',
                backgroundColor: bgActive,
                borderColor: bgActive,
            },
            ' .slider.round': {
                borderRadius: '34px',
            },
            ' .slider.round:before': {
                borderRadius: '50%',
            },
            ' body': {
                backgroundColor: '#f1f2f3'
            }
        },
        ' .text': {
            paddingLeft: 8,
            fontSize: 14,
            lineHeight: '1.29',
            color: text
        }
    });
};
