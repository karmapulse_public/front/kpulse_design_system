import React from 'react';
import PropTypes from 'prop-types';

import { css } from 'glamor';
import { generals, properties } from './styles';

const NavItem = ({
    onClick, bgColor, children, active
}) => {
    const button = () => (
        <button
            className={active ? 'active' : ''}
            onClick={evt => onClick(evt)}
            {...css(generals, properties(bgColor))}
        >
            {children}
        </button>
    );
    return button();
};

NavItem.propTypes = {
    bgColor: PropTypes.string,
    fontColor: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.node,
    active: PropTypes.bool
};

NavItem.defaultProps = {
    bgColor: '#233851',
    fontColor: '#ffffff',
    onClick: () => ({}),
    children: () => ({}),
    active: false
};

export default NavItem;
