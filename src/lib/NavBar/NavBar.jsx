import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { css } from 'glamor';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';

import { generals, bottomToButton, animate } from './styles';

const NavBar = ({ bgColor, children, closeButton, expanded }) => {
    const [state, setState] = useState({ toggle: false });
    // const x = window.matchMedia('(max-width: 1200px)');

    const dispatch = () => setState({ toggle: !state.toggle });

    // const listenMediaQuery = (x) => {
    //     console.log(x);
    //     if (x.matches) {
    //         dispatch(true);
    //     }
    //     // dispatch();
    // };

    // useEffect(() => {
    //     listenMediaQuery(x);
    //     x.addListener(listenMediaQuery);
    // });


    const buttonExpanded = () => {
        if (!closeButton) return null;
        return (
            <button {...css(bottomToButton)} onClick={dispatch} className="btn-toggle">
                <div className="button-close">
                    <div className="icons">
                        <FontAwesomeIcon icon={faChevronLeft} />
                        <FontAwesomeIcon icon={faChevronLeft} />
                    </div>
                </div>
            </button>
        );
    };
    const render = () => (
        <div {...css(generals(bgColor), animate(state.toggle, expanded))}>
            {children}
            {buttonExpanded()}
        </div>
    );
    return render();
};

NavBar.propTypes = {
    bgColor: PropTypes.string,
    fontColor: PropTypes.string,
    expanded: PropTypes.bool,
    children: PropTypes.node,
    closeButton: PropTypes.bool
};

NavBar.defaultProps = {
    bgColor: '#233851',
    fontColor: '#ffffff',
    expanded: false,
    children: () => ({}),
    closeButton: false
};

export default NavBar;
