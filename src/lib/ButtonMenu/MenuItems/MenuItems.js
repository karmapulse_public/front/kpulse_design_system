import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles';

const propTypes = {
    items: PropTypes.array,
    clicked: PropTypes.bool,
    onClickItem: PropTypes.func
};

const MenuItems = props => (
    <div
        {...styles(props)}
    >
        {props.items.map(item => (
            <button
                onClick={() => {
                    item.onClick();
                    props.onClickItem();
                    item.label();
                }}
            >{item.label}
            </button>
        ))}
    </div>
);

MenuItems.propTypes = propTypes;
MenuItems.defaultProps = {
    items: [],
    clicked: false,
    onClickItem: () => ({}),
};

export default MenuItems;
