import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles';
import MenuItems from './MenuItems';

class ButtonMenu extends Component {
    constructor(props) {
        super(props);
        this.handleOnClick = this.handleOnClick.bind(this);
        this.state = {
            clicked: false
        };
    }

    componentDidMount() {
        window.addEventListener(
            'resize',
            () => {
                this.setState({ clicked: false });
            }
        );
    }

    componentWillUnmount() {
        window.removeEventListener(
            'resize',
            () => {
                this.setState({ clicked: false });
            }
        );
    }

    handleOnClick() {
        this.setState({
            clicked: !this.state.clicked,
        });
    }

    render() {
        const { clicked } = this.state;
        const {
            children, listBgColor, itemHoverColor, listItems
        } = this.props;
        return (
            <div style={{ position: 'relative' }}>
                <button
                    {...styles()}
                    onClick={this.handleOnClick}
                >{children}
                </button>
                <MenuItems
                    clicked={clicked}
                    items={listItems}
                    bgColor={listBgColor}
                    hoverColor={itemHoverColor}
                    onClickItem={this.handleOnClick}
                />
            </div>
        );
    }
}

ButtonMenu.propTypes = {
    listItems: PropTypes.array,
    children: PropTypes.element,
    listBgColor: PropTypes.string,
    itemHoverColor: PropTypes.string
};
ButtonMenu.defaultProps = {
    children: 'Click me!',
    listBgColor: '#000',
    itemHoverColor: '#555',
    listItems: [
        { label: 'Menu 1', onClick: () => ({}) }
    ]
};

export default ButtonMenu;
