import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './styles';

const propTypes = {
    value: PropTypes.any,
    label: PropTypes.string,
    fontColor: PropTypes.string,
    bgColor: PropTypes.string,
    onChange: PropTypes.func,
    initChecked: PropTypes.bool,
    disabled: PropTypes.bool,
    reset: PropTypes.bool
};

const defaultProps = {
    value: 0,
    label: '',
    fontColor: '',
    bgColor: '',
    onChange: () => ({}),
    initChecked: false,
    disabled: false,
    reset: false
};

const Checkbox = (props) => {
    const [check, setCheck] = useState(props.initChecked);
    if (props.reset && check !== props.initChecked) {
        setCheck(props.initChecked);
    }
    return (
        <div
            {...props}
            {...styles(props.fontColor, props.bgColor)}
        >
            <input
                type="checkbox"
                disabled={props.disabled}
                checked={check}
                value={props.value}
                onClick={(e) => { props.onChange(e); setCheck(e.target.checked); }}
            />
            <span>{props.label}</span>
        </div>
    );
};

Checkbox.propTypes = propTypes;
Checkbox.defaultProps = defaultProps;

export default Checkbox;
