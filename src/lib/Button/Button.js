import React from 'react';
import PropTypes from 'prop-types';

import styles from './ButtonStyles';


const Button = props => (
    <button
        {...styles(props)}
        className="kp-button"
        onClick={props.onClick}
        style={props.style}
        disabled={props.disabled}
    >
        {props.children}
    </button>
);

Button.propTypes = {
    onClick: PropTypes.func,
    style: PropTypes.shape({}),
    children: PropTypes.element,
    round: PropTypes.bool,
    ghost: PropTypes.bool,
    small: PropTypes.bool,
    large: PropTypes.bool,
    disabled: PropTypes.bool,
    secondary: PropTypes.bool
};

Button.defaultProps = {
    onClick: () => ({}),
    style: {},
    children: null,
    small: false,
    large: false,
    round: false,
    ghost: false,
    disabled: false,
    secondary: false
};

export default Button;
