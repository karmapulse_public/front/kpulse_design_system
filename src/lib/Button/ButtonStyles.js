import { css } from 'glamor';

export default (props) => {
    let background;
    let fontColor;
    if (props.disabled) {
        background = props.secondary ? 'none' : '#576d88';
        fontColor = props.secondary ? '#576d88' : 'rgba(255, 255, 255, 0.2)';
    } else if (props.secondary) {
        background = 'none';
        fontColor = props.bgColor;
    } else if (props.ghost) {
        background = 'none';
        fontColor = props.bgColor;
    } else {
        background = props.bgColor;
        fontColor = '#FFF';
    }

    let height;
    let padding;
    if (props.small) {
        height = 20;
        padding = '0px 14px';
    } else if (props.large) {
        height = 40;
        padding = '0px 46px';
    } else {
        height = 32;
        padding = '0px 25px';
    }

    const borderColor = props.disabled ? '#576d88' : props.bgColor;

    return css({
        height,
        padding,
        background,
        color: fontColor,
        fontSize: props.small ? 12 : 14,
        border: props.ghost ? 'none' : `2px solid ${borderColor}`,
        borderRadius: props.round ? 15 : 0,
        transition: 'all 0.1s ease-in',
        cursor: props.disabled ? 'not-allowed' : 'pointer',
        outline: 'none',
        opacity: 1,
        boxSizing: 'border-box',
        display: 'flex',
        alignItems: 'center',
        ' > svg': {
            marginLeft: 5
        },
        '&:hover': {
            opacity: props.disabled ? 1 : 0.7
        }
    });
};
