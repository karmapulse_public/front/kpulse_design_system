import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import Typography from '../Typography';
import Button from '../Button';

import styles from './styles';

const { Title, Paragraph } = Typography;

const Modal = ({
    trigger,
    children,
    actions,
    defaultVisibility,
    fontColor,
    bgColor,
    lineColor,
    title,
    closeClick,
    controlButtom,
}) => {
    const [visible, setVisibility] = useState(defaultVisibility);

    const toogleVisibility = (value = undefined) => {
        setVisibility(value === undefined ? !visible : value);
    };

    const closeModal = (e) => {
        if (e.target.id === 'modal-bg' && closeClick) {
            toogleVisibility(false);
        }
    };

    return (
        <Fragment>
            {trigger(toogleVisibility)}
            <div {...styles(visible, bgColor, fontColor, lineColor)} onClick={closeModal} id="modal-bg" > 
                <div className={`modal-content ${actions.length > 0 ? 'modal-w-footer' : ''}`}>
                    {(() => {
                        if (closeClick) {
                            return (
                                <button
                                    onClick={() => toogleVisibility(false)}
                                >
                                    <FontAwesomeIcon icon={faTimes} />
                                </button>
                            );
                        }
                        return null;
                    })()}
                    {title ? <Title level={2} color={fontColor}>{title}</Title> : null}
                    <div className="modal-body" >
                        {children}
                    </div>
                    <div className="modal-footer">
                        {(() => {
                            if (closeClick) {
                                return (actions.map(item => item(toogleVisibility)));
                            }
                            return (
                                <Button
                                    bgColor="rgba(255, 255, 255, 0.1)"
                                    onClick={() => toogleVisibility(false)}
                                    style={{ visibility: `${controlButtom === true ? 'visible' : 'hidden'}` }}
                                >
                                    Aceptar
                                </Button>
                            );
                        })()}
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

Modal.defaultProps = {
    fontColor: '#fff',
    bgColor: '#1a3351',
    lineColor: '#58af6e',
    title: '',
    children: <Paragraph color="#FFF">Cuerpo Modal</Paragraph>,
    visible: false,
    trigger: undefined,
    defaultVisibility: false,
    actions: [],
    closeClick: true,
    controlButtom: true,
};

Modal.propTypes = {
    fontColor: PropTypes.string,
    bgColor: PropTypes.string,
    lineColor: PropTypes.string,
    title: PropTypes.string,
    children: PropTypes.node,
    visible: PropTypes.bool,
    trigger: PropTypes.node,
    closeClick: PropTypes.bool,
    controlButtom: PropTypes.bool,
    defaultVisibility: PropTypes.bool,
    actions: PropTypes.arrayOf(PropTypes.node)
};

export default Modal;
