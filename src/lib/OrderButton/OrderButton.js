import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';

import styles from './styles';


const OrderButton = (props) => {
    const [order, setOrder] = useState(props.defaultOrder);
    const onClick = () => {
        const newOrder = order === 'UP' ? 'DOWN' : 'UP';
        props.onClick(newOrder);
        setOrder(newOrder);
    };
    const newOrder = props.currentOrder !== ''
        ? props.currentOrder : order;

    return (
        <button
            {...styles(props)}
            style={props.style}
            disabled={props.disabled}
            className="kp-order-button"
            onClick={props.onChange ? props.onChange : onClick}
        >
            <span>{props.label}</span>
            <div>
                <FontAwesomeIcon
                    icon={faCaretUp}
                    className={newOrder === 'UP' ? 'arrow-selected' : 'arrow'}
                />
                <FontAwesomeIcon
                    icon={faCaretDown}
                    className={newOrder === 'DOWN' ? 'arrow-selected' : 'arrow'}
                />
            </div>
        </button>
    );
};

OrderButton.propTypes = {
    defaultOrder: PropTypes.string,
    currentOrder: PropTypes.string,
    onChange: PropTypes.any,
    onClick: PropTypes.func,
    style: PropTypes.shape({}),
    label: PropTypes.string,
    round: PropTypes.bool,
    ghost: PropTypes.bool,
    large: PropTypes.bool,
    disabled: PropTypes.bool,
    secondary: PropTypes.bool
};

OrderButton.defaultProps = {
    defaultOrder: 'UP',
    currentOrder: '',
    onChange: null,
    onClick: () => ({}),
    style: {},
    label: 'Click me!',
    large: false,
    round: false,
    ghost: false,
    disabled: false,
    secondary: false
};

export default OrderButton;
