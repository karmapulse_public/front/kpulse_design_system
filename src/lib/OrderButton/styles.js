import { css } from 'glamor';
import tinycolor from 'tinycolor2';

const colorTone = (color, amt) => {
    const col = tinycolor(color);

    if (amt < 0) {
        col.darken(amt * -1);
    } else {
        col.lighten(amt);
    }

    return col.toString('hex');
};

export default (props) => {
    let background;
    let fontColor;
    if (props.disabled) {
        background = props.secondary ? 'none' : '#576d88';
        fontColor = props.secondary ? '#576d88' : 'rgba(255, 255, 255, 0.2)';
    } else if (props.secondary) {
        background = 'none';
        fontColor = props.bgColor;
    } else if (props.ghost) {
        background = 'none';
        fontColor = props.bgColor;
    } else {
        background = props.bgColor;
        fontColor = '#FFF';
    }

    let height;
    let padding;
    if (props.large) {
        height = 40;
        padding = '0px 46px';
    } else {
        height = 32;
        padding = '2px 16px';
    }

    const borderColor = props.disabled ? '#576d88' : props.bgColor;

    return css({
        height,
        padding,
        background,
        fontSize: 14,
        color: fontColor,
        border: props.ghost ? 'none' : `2px solid ${borderColor}`,
        borderRadius: props.round ? 20 : 0,
        transition: 'all 0.1s ease-in',
        cursor: props.disabled ? 'not-allowed' : 'pointer',
        outline: 'none',
        boxSizing: 'content-box',
        display: 'flex',
        alignItems: 'center',
        ' > span': {
            marginBottom: 1
        },
        ' > div': {
            display: 'grid',
            marginLeft: 8,
            ' svg:last-child': {
                marginTop: -6
            }
        },
        ' .arrow': {
            ' path': {
                fill: colorTone(fontColor, -40),
            },
            '&-selected': {
                ' path': {
                    fill: fontColor
                }
            }
        }
    });
};
