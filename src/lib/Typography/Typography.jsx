import React from 'react';
import { css } from 'glamor';
import { titles, generals, weights, paragraph } from './styles';

const Title = ({
    level, children, bold, light, styles, color = '#000'
}) => {
    if (level > 6 || level < 1) return null;

    const weight = () => {
        if (bold) return 'bold';
        if (light) return 'light';
        return 'regular';
    };

    const levels = {
        1: () => <h1 {...css(generals(color), titles[`h${level}`], weights[weight()], styles)}>{children}</h1>,
        2: () => <h2 {...css(generals(color), titles[`h${level}`], weights[weight()], styles)}>{children}</h2>,
        3: () => <h3 {...css(generals(color), titles[`h${level}`], weights[weight()], styles)}>{children}</h3>,
        4: () => <h4 {...css(generals(color), titles[`h${level}`], weights[weight()], styles)}>{children}</h4>,
        5: () => <h5 {...css(generals(color), titles[`h${level}`], weights[weight()], styles)}>{children}</h5>,
        6: () => <h6 {...css(generals(color), titles[`h${level}`], weights[weight()], styles)}>{children}</h6>,
    };

    return levels[level]();
};

const Paragraph = ({
    small, large, children, bold, light, styles, color = '#000'
}) => {
    const weight = () => {
        if (bold) return 'bold';
        if (light) return 'light';
        return '';
    };

    const size = () => {
        if (small) return 'small';
        if (large) return 'large';
        return 'regular';
    };
    const Render = () => (
        <p {...css(generals(color), paragraph[size()], weights[weight()], styles)}>{children}</p>
    );

    return Render();
};

export default {
    Title,
    Paragraph
};
