import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
import Checkbox from '../Checkbox';

import styles from './styles';

class CheckboxList extends Component {
    constructor(props) {
        super(props);
        this.handleOnClick = this.handleOnClick.bind(this);
        this.hideList = this.hideList.bind(this);
        this.hideOnOutClick = this.hideOnOutClick.bind(this);
        const defaultItem = props.valueAllItem.find(i => i[props.id] === props.defaultItem) || props.listItems.find(i => i[props.id] === props.defaultItem);
        this.me = React.createRef();
        this.state = {
            clicked: false,
            selectedItem: defaultItem ? defaultItem[props.value] : '',
            toolData: '',

        };
    }

    componentDidMount() {
        window.addEventListener(
            'resize',
            () => this.hideList(false)
        );
        window.addEventListener(
            'click',
            this.hideOnOutClick
        );
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.valueAllItem !== this.props.valueAllItem || (nextProps.reset !== this.props.reset && nextProps.reset) || nextProps.listItems !== this.props.listItems || (nextProps.reset !== this.props.reset && nextProps.reset)) {
            const defaultItem = nextProps.valueAllItem.find(i => i[this.props.id] === nextProps.defaultItem) || nextProps.listItems.find(i => i[this.props.id] === nextProps.defaultItem);
            this.setState({
                clicked: false,
                selectedItem: defaultItem ? defaultItem[nextProps.value] : ''
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener(
            'resize',
            () => this.hideList(false)
        );
        window.removeEventListener(
            'click',
            this.hideOnOutClick
        );
    }

    hideOnOutClick(e) {
        if (!this.me.current.contains(e.target)) {
            this.hideList(false);
        }
    }


    hideList(setValue = null) {
        this.setState({
            clicked: setValue === null ? !this.state.clicked : setValue
        });
    }

    handleOnClick(item) {
        this.props.onClick(item);
        this.setState({
            selectedItem: item[this.props.value],
        });
    }
    dataTooltip(e) {
        // console.log(e.nativeEvent);
        this.setState({
            toolData: e.nativeEvent.srcElement.innerText,
        });
    }

    render() {
        const { clicked, selectedItem, toolData } = this.state;
        const {
            listItems,
            disabled,
            enableTooltip
        } = this.props;
        const visible = clicked === false ? 'closed' : 'opened';
        const disabClass = disabled === false ? '' : 'disabled';
        const tooltip = (e) => {
            const enable = e;
            return (
                <div>
                    {
                        enable.length > 30 ? <ReactTooltip className="extraClass" place="bottom" type="light" effect="float" /> : ''
                    }
                </div>
            );
        };

        
        return (
            <div
                {...styles(this.props)}
                ref={this.me}
                className={`select select--${visible} select--${disabClass}`}
            >
                <button
                    onClick={() => this.hideList()}
                >
                    {selectedItem}
                </button>
                <div className="dataList">
                    <div className="listHeader">
                        <div className="container">
                            <Checkbox
                                value={this.props.valueAllItem[0].id}
                                label={this.props.valueAllItem[0].key}
                                fontColor="#000"
                                bgColor="#58af6e"
                                onChange={e => console.log(e.target)}
                            />
                        </div>
                    </div>
                    <div className="listChekbok">
                        {
                            listItems.map(item => (
                                <div key={item.id}>
                                    <Checkbox
                                        value={item[this.props.value]}
                                        label={item[this.props.value]}
                                        fontColor="#000"
                                        bgColor="#58af6e"
                                        onChange={e => console.log(e.target)}
                                    />
                                </div>
                            ))
                        }
                    </div>
                    <div className="listButtons">
                        <button>
                            Cancelar
                        </button>
                        <button >
                            Aceptar
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}


CheckboxList.propTypes = {
    onClick: PropTypes.func,
    listItems: PropTypes.array,
    valueAllItem: PropTypes.array,
    children: PropTypes.any,
    id: PropTypes.string,
    value: PropTypes.string,
    listBgColor: PropTypes.string,
    itemHoverColor: PropTypes.string,
    arrowColor: PropTypes.string,
    defaultItem: PropTypes.any,
    disabled: PropTypes.bool,
    enableTooltip: PropTypes.bool,
    reset: PropTypes.bool
};

CheckboxList.defaultProps = {
    onClick: () => ({}),
    id: 'id',
    value: 'value',
    children: 'Click me!',
    listBgColor: '#000',
    itemHoverColor: '#555',
    arrowColor: '#f5f5f5',
    listItems: [
        { label: 'Menu 1', id: 'menu1' }
    ],
    valueAllItem: [
        { label: 'Todos', id: 'all' }
    ],
    defaultItem: -1,
    disabled: false,
    enableTooltip: false,
    reset: false
};

export default CheckboxList;
