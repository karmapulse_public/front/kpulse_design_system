 import { css } from 'glamor';

export default (props) => {
    const listcolor = props.listBgColor;
    const hoverColor = props.itemHoverColor;
    const textcolor = props.textColor;
    const arrowcolor = props.arrowColor;

    return css({
        fontFamily: 'Roboto, sans-serif',
        '.select': {
            position: 'relative',
            height: '32px',
            background: listcolor,
            width: 358,
            '&--opened': {
                ' > .dataList': {
                    top: '100%', transform: 'scale(1)', opacity: 1, transition: 'all 0.25s ease, opacity 0.15s ease 0.10s',
                },
                ' > button:after': { transform: 'translate(0, -50%) rotate(180deg)' }
            },
            '&--closed': {
                ' > .dataList': {
                    top: '0px', transform: 'scale(0)', opacity: 0, transition: 'all 0.20s ease'
                },
                ' > div': {
                    ' > div': {
                        visibility: 'hidden'
                    },
                },
                ' > button:after': { transform: 'translate(0, -50%) rotate(0deg)' }
            },
            '&--disabled': {
                ':after': {
                    content: '""',
                    position: 'absolute',
                    top: '0px',
                    left: '0px',
                    zIndex: 1,
                    width: '100%',
                    height: '100%',
                    background: 'rgba(0,0,0,0.4)'
                }
            },
            ' >button': {
                width: '100%',
                height: 32,
                color: textcolor,
                border: 'none',
                outline: 'none',
                paddingLeft: 16,
                textAlign: 'left',
                position: 'relative',
                backgroundColor: listcolor,
                fontSize: '14px',
                fontFamily: 'Roboto, sans-serif',
                cursor: 'pointer',
                fontWeight: 500,
            },
            ' .dataList': {
                fontFamily: 'Roboto, sans-serif',
                zIndex: 2,
                left: '0px',
                width: 758,
                margin: 0,
                fontWeight: 500,
                position: 'absolute',
                ' .listHeader': {
                    backgroundColor: listcolor,
                    padding: '12px 20px',
                    position: 'relative',
                    '&:after': {
                        content: '""',
                        position: 'absolute',
                        bottom: '0%',
                        left: '50%',
                        width: '713px',
                        height: '1px',
                        backgroundColor: '#e4e4e4',
                        transform: 'translate(-50%, -50%)'
                    },
                    ' .container': {
                        width: 228,
                        height: 38,
                        display: 'flex',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        ' >div': {
                            width: '100%',
                        },
                    },
                },
                ' .listChekbok': {
                    columnCount: 3,
                    padding: '12px 20px',
                    backgroundColor: listcolor,
                    ' >div': {
                        padding: '6.5px 6.5px',
                    },
                },
                ' .listButtons': {
                    height: 53,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    backgroundColor: 'rgb(241, 241, 241)',
                    ' >button': {
                        width: 73,
                        height: 31,
                        border: 'none',
                        outline: 'none',
                        cursor: 'pointer',
                        fontSize: '14px',
                        ':nth-child(1)': {
                            background: 'transparent'
                        },
                        ':nth-child(2)': {
                            background: hoverColor,
                            marginLeft: 16,
                            marginRight: 20,
                            color: '#ffff',
                        },
                    }
                },
            },
            ' .extraClass': {
                fontSize: '9px',
                opacity: '0.5',
                padding: ' 8px 8px'
            }
        },
    });
};
