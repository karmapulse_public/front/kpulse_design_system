import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './styles';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

const propTypes = {
    label: PropTypes.string,
    close: PropTypes.bool,
    fontColor: PropTypes.string,
    bgColor: PropTypes.string,
    onClose: PropTypes.func
};

const defaultProps = {
    label: '',
    close: true,
    fontColor: '',
    bgColor: '',
    onClose: () => ({})
};

const Chip = (props) => {
    const [show, setShow] = useState(true);

    const close = () => (
        <button onClick={() => { setShow(false); props.onClose(props.label); }}>
            <FontAwesomeIcon icon={faTimes} />
        </button>
    );

    if (show === false) {
        return null;
    }

    return (
        <div
            {...props}
            {...styles(props.fontColor, props.bgColor)}
        >
            <div>
                <span>{props.label}</span>
                {
                    props.close ? close() : null
                }
            </div>
        </div>
    );
};

Chip.propTypes = propTypes;
Chip.defaultProps = defaultProps;

export default Chip;
