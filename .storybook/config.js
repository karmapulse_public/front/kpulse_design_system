import { configure, setAddon, addDecorator } from '@storybook/react';
import { setOptions } from '@storybook/addon-options'
import centered from '@storybook/addon-centered';
import { withInfo } from '@storybook/addon-info';
import { addParameters } from '@storybook/react';
import { create } from '@storybook/theming';

// console.log(logo);

addDecorator(withInfo);
addDecorator(centered);

const req = require.context('../src/stories/', true, /stories\.jsx$/)

function loadStories() {
	req.keys().forEach(req)
}

// addParameters({
// 	options: {
// 		name: 'asasd',
// 		isFullScreen: false,
// 		showPanel: true,
// 		showAddonsPanel: true,
// 		panelPosition: 'bottom',
// 		// more configuration here
// 	},
// });
addParameters({
	options: {
		theme: create({
			base: 'dark',
			brandTitle: 'Design System',
			brandUrl: 'https://karmapulse.com',
			brandImage: 'http://cdn.karmapulse.com/KarmaDS-claro.png',
	  
			// // UI
			// appBg: 'white',
			// appContentBg: 'silver',
			// appBorderColor: 'grey',
			appBorderRadius: 4,
	  
			// Typography
			fontBase: '"Roboto", sans-serif',
			fontCode: 'monospace',
		  })
	},
});

configure(loadStories, module);