import { css } from 'glamor';

const generals = color => css({
    padding: 0,
    margin: 0,
    fontFamily: 'inherit',
    color
});

const titles = {
    h1: css({
        fontSize: '1.75rem',
        fontWeight: '400',
        lineHeight: '1.25'
    }),
    h2: css({
        fontSize: '1.5rem',
        fontWeight: '400',
        lineHeight: '1.25'
    }),
    h3: css({
        fontSize: '1.25rem',
        fontWeight: '400',
        lineHeight: '1.25'
    }),
    h4: css({
        fontSize: '1.125rem',
        fontWeight: '400',
        lineHeight: '1.25'
    }),
    h5: css({
        fontSize: '1rem',
        fontWeight: '400',
        lineHeight: '1.25'
    }),
    h6: css({
        fontSize: '0.875rem',
        fontWeight: '400',
        lineHeight: '1.25'
    })
};

const weights = {
    bold: css({
        fontWeight: '700'
    }),
    light: css({
        fontWeight: '300'
    }),
    regular: css({
        fontWeight: '400'
    })
};

const paragraph = {
    regular: css({
        fontSize: '0.875rem',
        fontWeight: '400',
        lineHeight: '1.5'
    }),
    small: css({
        fontSize: '0.6875rem',
        fontWeight: '400',
        lineHeight: '1.5'
    }),
    large: css({
        fontSize: '1rem',
        fontWeight: '400',
        lineHeight: '1.5'
    })
};

export { generals, titles, weights, paragraph };