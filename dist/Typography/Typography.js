import React from 'react';
import { css } from 'glamor';
import { titles, generals, weights, paragraph } from './styles';

const Title = ({
    level, children, bold, light, styles, color = '#000'
}) => {
    if (level > 6 || level < 1) return null;

    const weight = () => {
        if (bold) return 'bold';
        if (light) return 'light';
        return 'regular';
    };

    const levels = {
        1: () => React.createElement(
            'h1',
            css(generals(color), titles[`h${level}`], weights[weight()], styles),
            children
        ),
        2: () => React.createElement(
            'h2',
            css(generals(color), titles[`h${level}`], weights[weight()], styles),
            children
        ),
        3: () => React.createElement(
            'h3',
            css(generals(color), titles[`h${level}`], weights[weight()], styles),
            children
        ),
        4: () => React.createElement(
            'h4',
            css(generals(color), titles[`h${level}`], weights[weight()], styles),
            children
        ),
        5: () => React.createElement(
            'h5',
            css(generals(color), titles[`h${level}`], weights[weight()], styles),
            children
        ),
        6: () => React.createElement(
            'h6',
            css(generals(color), titles[`h${level}`], weights[weight()], styles),
            children
        )
    };

    return levels[level]();
};

const Paragraph = ({
    small, large, children, bold, light, styles, color = '#000'
}) => {
    const weight = () => {
        if (bold) return 'bold';
        if (light) return 'light';
        return '';
    };

    const size = () => {
        if (small) return 'small';
        if (large) return 'large';
        return 'regular';
    };
    const Render = () => React.createElement(
        'p',
        css(generals(color), paragraph[size()], weights[weight()], styles),
        children
    );

    return Render();
};

export default {
    Title,
    Paragraph
};