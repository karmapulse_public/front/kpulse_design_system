import { css } from 'glamor';

export default (props => css({
    position: 'relative',
    margin: '10px',
    ' input': {
        background: 'transparent',
        width: '100%',
        minWidth: '260px',
        minHeight: '32px',
        transition: 'all 0.3s ease',
        border: '1px solid transparent',
        color: props.fontColor,
        outline: 'none',
        paddingLeft: '32px',
        paddingRight: '25px',
        boxSizing: 'border-box',
        '&::placeholder': {
            opacity: 0.5,
            paddingLeft: '2px'
        },
        '&:focus': {
            border: `1px solid ${props.borderFocus}`,
            opacity: 1
        }
    },
    ' > svg:nth-child(2)': {
        position: 'absolute',
        top: '50%',
        left: '10px',
        transform: 'translate(0, -50%)',
        ' path': {
            opacity: 0.5,
            color: props.colorIcon
        }
    },
    ' input:focus + svg:nth-child(2) > path': {
        opacity: 1
    },

    ' > svg:nth-child(3)': {
        position: 'absolute',
        top: '50%',
        right: '10px',
        transform: 'translate(0, -50%)',
        cursor: 'pointer',
        opacity: 0.5,
        ' path': {
            fill: props.colorIcon
        }
    }
}));