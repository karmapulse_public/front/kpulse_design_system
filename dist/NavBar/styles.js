import { css } from 'glamor';

const closeAnimate = css.keyframes('closeAnimate', {
    '0%': {
        opacity: 1,
        right: 0
    },
    '50%': {
        opacity: 0,
        right: 15
    },
    '100%': {
        opacity: 0,
        right: 0
    }
});

const generals = bgColor => css({
    position: 'relative',
    width: 200,
    height: '100%',
    paddingTop: 30,
    boxShadow: '0 2px 15px 0 rgba(0, 0, 0, 0.3)',
    backgroundColor: bgColor,
    boxSizing: 'border-box',
    transition: 'all 0.2s ease-in',
    overflow: 'hidden'

});

const bottomToButton = css({
    position: 'absolute',
    bottom: 0,
    width: 200,
    height: 45,
    paddingLeft: 20,
    paddingRight: 20,
    border: 'none',
    borderRadius: 0,
    backgroundColor: 'rgba(256, 226, 226, 0.1)',
    color: '#fff',
    fontSize: '14px',
    fontWeight: 400,
    textAlign: 'right',
    overflow: 'hidden',
    cursor: 'pointer',
    transition: 'all 0.1s ease-in',

    ' &:focus': {
        outline: 'none'
    },

    ' & .button-close': {
        position: 'relative',
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        transition: 'all 0.2s ease-in',

        ' & .icons': {
            position: 'absolute',
            transition: 'all 0.2s ease-in',

            ' & svg.svg-inline--fa': {
                position: 'absolute',
                right: 0,
                top: -6,

                ' &:last-child': {
                    right: 0
                }
            }

        },

        ' &:hover .icons svg.svg-inline--fa:last-child': {
            animationDuration: '0.8s',
            animationName: `${closeAnimate}`,
            animationIterationCount: 'infinite'
        }
    }
});

const animate = (arg, expanded) => css({
    width: arg ? 50 : 200,

    ' button': {
        '& .button-close .icons': {
            transform: arg ? 'rotateY(180deg) translateX(158px)' : 'rotateY(0deg) translateX(0)',
            justifyContent: arg ? 'flex-start' : 'flex-end'
        }
    },

    '@media screen and (max-width: 1200px)': {
        width: expanded ? 'initial' : 50,
        ' button.btn-toggle': {
            display: 'none'
        }
    }

});
export { generals, animate, bottomToButton };