var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { css } from 'glamor';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';

import { generals, bottomToButton, animate } from './styles';

const NavBar = ({ bgColor, children, closeButton, expanded }) => {
    const [state, setState] = useState({ toggle: false });
    // const x = window.matchMedia('(max-width: 1200px)');

    const dispatch = () => setState({ toggle: !state.toggle });

    // const listenMediaQuery = (x) => {
    //     console.log(x);
    //     if (x.matches) {
    //         dispatch(true);
    //     }
    //     // dispatch();
    // };

    // useEffect(() => {
    //     listenMediaQuery(x);
    //     x.addListener(listenMediaQuery);
    // });


    const buttonExpanded = () => {
        if (!closeButton) return null;
        return React.createElement(
            'button',
            _extends({}, css(bottomToButton), { onClick: dispatch, className: 'btn-toggle' }),
            React.createElement(
                'div',
                { className: 'button-close' },
                React.createElement(
                    'div',
                    { className: 'icons' },
                    React.createElement(FontAwesomeIcon, { icon: faChevronLeft }),
                    React.createElement(FontAwesomeIcon, { icon: faChevronLeft })
                )
            )
        );
    };
    const render = () => React.createElement(
        'div',
        css(generals(bgColor), animate(state.toggle, expanded)),
        children,
        buttonExpanded()
    );
    return render();
};

NavBar.propTypes = {
    bgColor: PropTypes.string,
    fontColor: PropTypes.string,
    expanded: PropTypes.bool,
    children: PropTypes.node,
    closeButton: PropTypes.bool
};

NavBar.defaultProps = {
    bgColor: '#233851',
    fontColor: '#ffffff',
    expanded: false,
    children: () => ({}),
    closeButton: false
};

export default NavBar;