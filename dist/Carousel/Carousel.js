var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React, { Children, useState, Fragment } from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';

import styles from './styles';

const dots = (withDots, dotsPosition, childCount, value, handleChangePos) => {
    const renderDots = React.createElement(
        'div',
        { className: `carousel-ctrDots carousel-ctrDots--${dotsPosition}` },
        (() => {
            const items = [];
            for (let i = 0; i < childCount; i += 1) {
                let cb = '';
                if (value === i) cb = 'carousel-ctrDots__dot--active';
                items.push(React.createElement('button', {
                    key: `cd__${i}`,
                    onClick: () => handleChangePos(i),
                    className: `carousel-ctrDots__dot ${cb}`
                }));
            }
            return items;
        })()
    );
    return withDots ? renderDots : null;
};

const arrows = (withArrows, arrowsPosition, childCount, value, handleChangePos) => {
    const renderArrows = React.createElement(
        Fragment,
        null,
        React.createElement(
            'button',
            {
                className: `carousel-ctrArrow carousel-ctrArrow__left carousel-ctrArrow--${arrowsPosition} carousel-ctrArrow--${value === 0 ? 'inactive' : ''}`,
                onClick: () => handleChangePos(value - 1)
            },
            React.createElement(FontAwesomeIcon, { icon: faChevronLeft, fixedWidth: true })
        ),
        React.createElement(
            'button',
            {
                className: `carousel-ctrArrow carousel-ctrArrow__right carousel-ctrArrow--${arrowsPosition} carousel-ctrArrow--${value === childCount ? 'inactive' : ''}`,
                onClick: () => handleChangePos(value + 1)
            },
            React.createElement(FontAwesomeIcon, { icon: faChevronRight, fixedWidth: true })
        )
    );
    return withArrows ? renderArrows : null;
};

const Carousel = ({
    children,
    defaultItem,
    width,
    height,
    onChange,
    arrowsColor,
    dotsColor,
    withDots,
    withArrows,
    arrowsPosition,
    dotsPosition,
    arrowsHeight
}) => {
    const [value, setValue] = useState(defaultItem !== -1 ? defaultItem : 0);
    const childCount = Children.count(children);

    const handleChangePos = pos => {
        let newPos = pos;
        if (pos < 0) newPos = 0;else if (pos >= childCount) newPos = childCount - 1;
        onChange(newPos);
        setValue(newPos);
    };

    return React.createElement(
        'div',
        _extends({ className: 'carousel' }, styles(width, height, arrowsColor, dotsColor, dotsPosition, withDots, arrowsHeight)),
        React.createElement(
            'div',
            { className: 'carousel-items' },
            Children.map(children, (child, index) => {
                let cn = 'inactive-right';
                if (value === index) cn = 'active';else if (value > index) cn = 'inactive-left';
                return React.createElement(
                    'div',
                    { key: `ci__${index}`, className: `carousel-items__item carousel-items__item--${cn}` },
                    child
                );
            })
        ),
        dots(withDots, dotsPosition, childCount, value, handleChangePos),
        arrows(withArrows, arrowsPosition, childCount - 1, value, handleChangePos)
    );
};

Carousel.propTypes = {
    width: PropTypes.string,
    height: PropTypes.string,
    children: PropTypes.any,
    defaultItem: PropTypes.number,
    visualization: PropTypes.string,
    onChange: PropTypes.func,
    arrowsColor: PropTypes.string,
    dotsColor: PropTypes.string,
    withDots: PropTypes.bool,
    withArrows: PropTypes.bool,
    arrowsHeight: PropTypes.string,
    arrowsPosition: PropTypes.string,
    dotsPosition: PropTypes.string
};

Carousel.defaultProps = {
    children: 'Click me',
    defaultItem: -1,
    width: '260px',
    height: '120px',
    visualization: 'v1',
    onChange: () => {},
    arrowsColor: '#58af6e',
    dotsColor: '#58af6e',
    withDots: true,
    withArrows: true,
    arrowsHeight: '15px',
    arrowsPosition: 'side', // top, bottom, side
    dotsPosition: 'bottom' // top, bottom, left, right
};

export default Carousel;