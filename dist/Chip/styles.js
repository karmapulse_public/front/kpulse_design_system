import { css } from 'glamor';

export default ((fontColor, bgColor) => css({
    ' div': {
        position: 'relative',
        display: 'flex',
        minHeight: '20px',
        padding: '5px 0px',
        margin: '0px 15px',
        background: bgColor,
        alignItems: 'center',
        justifyContent: 'flex-end',
        transition: 'all 0.2s ease',
        ' span': {
            zIndex: 1,
            fontFamily: 'Roboto',
            color: fontColor,
            fontSize: '11px'
        },
        ' button': {
            zIndex: 1,
            width: 'auto',
            height: 'auto',
            padding: 0,
            marginLeft: '5px',
            border: 'none',
            background: 'transparent',
            outline: 'none',
            ':hover': {
                ' svg': {
                    opacity: 1
                }
            }
        },
        ' svg': {
            opacity: 0.5,
            cursor: 'pointer',
            transition: 'all 0.25s ease-in',
            ' path': {
                fill: fontColor
            }
        },
        '&:after, &:before': {
            position: 'absolute',
            content: '""',
            top: '0px',
            zIndex: 0,
            width: '30px',
            height: '100%',
            background: bgColor,
            borderRadius: '50%',
            transition: 'all 0.2s ease'
        },
        ':after': {
            left: '0px',
            transform: 'translate(-50%, 0px)'
        },
        ':before': {
            right: '0px',
            transform: 'translate(50%, 0px)'
        },
        '&:hover, &:hover:after, &:hover:before': {
            background: '#7d96b4'
        }
    }
}));