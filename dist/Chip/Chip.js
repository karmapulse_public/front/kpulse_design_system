var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './styles';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

const propTypes = {
    label: PropTypes.string,
    close: PropTypes.bool,
    fontColor: PropTypes.string,
    bgColor: PropTypes.string,
    onClose: PropTypes.func
};

const defaultProps = {
    label: '',
    close: true,
    fontColor: '',
    bgColor: '',
    onClose: () => ({})
};

const Chip = props => {
    const [show, setShow] = useState(true);

    const close = () => React.createElement(
        'button',
        { onClick: () => {
                setShow(false);props.onClose(props.label);
            } },
        React.createElement(FontAwesomeIcon, { icon: faTimes })
    );

    if (show === false) {
        return null;
    }

    return React.createElement(
        'div',
        _extends({}, props, styles(props.fontColor, props.bgColor)),
        React.createElement(
            'div',
            null,
            React.createElement(
                'span',
                null,
                props.label
            ),
            props.close ? close() : null
        )
    );
};

Chip.propTypes = propTypes;
Chip.defaultProps = defaultProps;

export default Chip;