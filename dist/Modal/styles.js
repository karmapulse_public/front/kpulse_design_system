import { css } from 'glamor';

export default ((visible, bgColor, fontColor, lineColor) => css({
    position: 'fixed',
    top: 0,
    left: 0,
    display: visible ? 'block' : 'none',
    width: '100vw',
    height: '100vh',
    border: 'none',
    backgroundColor: 'rgba(0,0,0,0.8)',
    outline: 'none',
    zIndex: 1000,
    ' .modal': {
        '&-content': {
            position: 'relative',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 500,
            paddingTop: '45px',
            color: fontColor,
            backgroundColor: bgColor,
            boxShadow: '0px 12px 24px 0px rgba(0, 0, 0, 0.1)',
            ' > h2': {
                padding: '0px 50px',
                marginBottom: '15px'
            },
            ' .modal-body': {
                padding: '0px 50px 45px'
            },
            ' > button': {
                position: 'absolute',
                top: '15px',
                right: '10px',
                border: 'none',
                backgroundColor: 'transparent',
                cursor: 'pointer'
            },
            ' > button > svg > path': {
                fill: fontColor
            },
            ':after': {
                content: '""',
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: 2,
                backgroundColor: lineColor
            }
        },
        '&-w-footer': {
            ' .modal-body': {
                padding: '0px 50px 65px'
            },
            ' .modal-footer': {
                display: 'flex'
            }
        },
        '&-footer': {
            display: 'none',
            alignItems: 'center',
            justifyContent: 'flex-end',
            padding: '25px',
            backgroundColor: 'rgba(255, 255, 255, 0.1)',
            ' > button:not(:last-child)': {
                marginRight: 15
            }
        }
    }
}));