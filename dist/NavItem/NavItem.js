var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React from 'react';
import PropTypes from 'prop-types';

import { css } from 'glamor';
import { generals, properties } from './styles';

const NavItem = ({
    onClick, bgColor, children, active
}) => {
    const button = () => React.createElement(
        'button',
        _extends({
            className: active ? 'active' : '',
            onClick: evt => onClick(evt)
        }, css(generals, properties(bgColor))),
        children
    );
    return button();
};

NavItem.propTypes = {
    bgColor: PropTypes.string,
    fontColor: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.node,
    active: PropTypes.bool
};

NavItem.defaultProps = {
    bgColor: '#233851',
    fontColor: '#ffffff',
    onClick: () => ({}),
    children: () => ({}),
    active: false
};

export default NavItem;