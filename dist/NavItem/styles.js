import { css } from 'glamor';

const generals = css({
    position: 'relative',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 200,
    height: 45,
    paddingLeft: 20,
    paddingRight: 20,
    border: 'none',
    borderRadius: 0,
    backgroundColor: 'transparent',
    color: '#fff',
    fontSize: '14px',
    fontWeight: 400,
    textAlign: 'left',
    overflow: 'hidden',
    cursor: 'pointer',
    transition: 'all 0.1s ease-in',

    ' &:before': {
        position: 'absolute',
        content: '""',
        top: 0,
        left: 0,
        width: 4,
        height: '100%',
        backgroundColor: 'transparent',
        transition: 'all 0.1s ease-in'
    },

    ' &:after': {
        position: 'absolute',
        content: '""',
        top: 0,
        right: 0,
        width: 196,
        height: '100%',
        transition: 'all 0.1s ease-in'
    },

    ' &:focus': {
        outline: 'none'
    },

    ' svg': {
        transition: 'all 0.2s ease-in',
        ' path': {
            opacity: '0.5'
        }
    }
});

const properties = bgColor => css({
    ' &:hover': {
        fontWeight: 700,
        ' &:after': {
            backgroundColor: 'rgba(256, 226, 226, 0.1)'
        },

        ' &:before': {
            backgroundColor: bgColor
        },

        ' & svg.svg-inline--fa path': {
            opacity: 1
        }
    },

    ' &.active': {
        fontWeight: 700,
        ' &:after': {
            backgroundColor: 'rgba(256, 226, 226, 0.1)'
        },

        ' &:before': {
            backgroundColor: bgColor
        },

        ' & svg.svg-inline--fa path': {
            opacity: 1
        }
    }
});

export { generals, properties };