var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React from 'react';
import PropTypes from 'prop-types';

import { TabStyles } from './styles';

const propTypes = {
    label: PropTypes.string,
    icon: PropTypes.func,
    fontColor: PropTypes.string.isRequired,
    bgColor: PropTypes.string.isRequired,
    small: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
    isActive: PropTypes.bool.isRequired
};

const defaultProps = {
    label: '',
    icon: () => null
};

const Tab = props => {
    const {
        label,
        onChange,
        icon,
        bgColor,
        fontColor,
        small,
        isActive
    } = props;

    return React.createElement(
        'button',
        _extends({}, TabStyles(fontColor, bgColor, small), {
            type: 'button',
            className: `tab ${isActive ? 'tab--active' : 'tab--inactive'}`,
            onClick: () => onChange()
        }),
        icon(),
        label
    );
};

Tab.propTypes = propTypes;
Tab.defaultProps = defaultProps;

export default Tab;