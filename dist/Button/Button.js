var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React from 'react';
import PropTypes from 'prop-types';

import styles from './ButtonStyles';

const Button = props => React.createElement(
    'button',
    _extends({}, styles(props), {
        className: 'kp-button',
        onClick: props.onClick,
        style: props.style,
        disabled: props.disabled
    }),
    props.children
);

Button.propTypes = {
    onClick: PropTypes.func,
    style: PropTypes.shape({}),
    children: PropTypes.element,
    round: PropTypes.bool,
    ghost: PropTypes.bool,
    small: PropTypes.bool,
    large: PropTypes.bool,
    disabled: PropTypes.bool,
    secondary: PropTypes.bool
};

Button.defaultProps = {
    onClick: () => ({}),
    style: {},
    children: null,
    small: false,
    large: false,
    round: false,
    ghost: false,
    disabled: false,
    secondary: false
};

export default Button;