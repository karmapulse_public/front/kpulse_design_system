import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    items: PropTypes.array,
    value: PropTypes.string,
    onClickItem: PropTypes.func,
    onChange: PropTypes.func
};

const MenuItems = props => props.items.map(item => React.createElement(
    'li',
    { key: `${Math.random()}-${item[props.value]}` },
    React.createElement(
        'button',
        {
            onClick: () => {
                props.onChange(item);
                props.onClickItem();
            }
        },
        item[props.value]
    )
));

MenuItems.propTypes = propTypes;
MenuItems.defaultProps = {
    items: [],
    value: 'value',
    onClickItem: () => ({}),
    onChange: () => ({})
};

export default MenuItems;