var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MenuItems from '../MenuItems';

import styles from './styles';

class Datalist extends Component {
    constructor(props) {
        super(props);
        this.handleOnClick = this.handleOnClick.bind(this);
        this.hideList = this.hideList.bind(this);
        this.hideOnOutClick = this.hideOnOutClick.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
        this.me = React.createRef();
        const defaultItem = props.listItems.find(i => i[props.id] === props.defaultItem);
        this.state = {
            clicked: false,
            listItems: props.listItems,
            selectedItem: defaultItem ? defaultItem[props.value] : '',
            prevItem: ''
        };
    }

    componentDidMount() {
        window.addEventListener('resize', () => this.hideList(false));
        window.addEventListener('click', this.hideOnOutClick);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.listItems !== this.props.listItems || nextProps.reset !== this.props.reset && nextProps.reset) {
            const defaultItem = nextProps.listItems.find(i => i[this.props.id] === nextProps.defaultItem);
            this.setState({
                clicked: false,
                listItems: nextProps.listItems,
                selectedItem: defaultItem ? defaultItem[nextProps.value] : ''
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', () => this.hideList(false));
        window.removeEventListener('click', this.hideOnOutClick);
    }

    onChangeText(e) {
        const newList = this.props.listItems.filter(i => i[this.props.value].toString().toLowerCase().includes(e.target.value.toLowerCase()));
        this.setState({
            listItems: newList,
            selectedItem: e.target.value
        });
    }

    hideOnOutClick(e) {
        if (this.state.clicked) {
            if (!this.me.current.contains(e.target)) {
                this.hideList(false);
                this.setState({
                    clicked: false,
                    selectedItem: this.state.prevItem,
                    prevItem: ''
                });
            }
        }
    }

    hideList(setValue = null) {
        const newValue = setValue === null ? !this.state.clicked : setValue;
        if (newValue) {
            this.setState({
                clicked: newValue,
                selectedItem: '',
                prevItem: this.state.selectedItem,
                listItems: this.props.listItems
            });
        } else {
            this.setState({
                clicked: newValue
            });
        }
    }

    handleOnClick(item) {
        this.props.onClick(item);
        this.setState({
            selectedItem: item[this.props.value]
        });
    }

    render() {
        const { clicked, selectedItem, listItems } = this.state;
        const visible = clicked === false ? 'closed' : 'opened';
        const disabled = this.props.disabled === false ? '' : 'disabled';
        return React.createElement(
            'div',
            _extends({}, styles(this.props), {
                ref: this.me,
                className: `select select--${visible} select--${disabled}`
            }),
            React.createElement(
                'div',
                null,
                React.createElement('input', {
                    type: 'text',
                    onClick: this.hideList,
                    onChange: this.onChangeText,
                    value: selectedItem,
                    placeholder: this.props.placeholder
                })
            ),
            React.createElement(
                'ul',
                null,
                React.createElement(MenuItems, {
                    value: this.props.value,
                    items: listItems,
                    onChange: this.handleOnClick,
                    onClickItem: () => this.hideList()
                })
            )
        );
    }
}

Datalist.propTypes = {
    onClick: PropTypes.func,
    listItems: PropTypes.array,
    children: PropTypes.element,
    placeholder: PropTypes.string,
    id: PropTypes.string,
    value: PropTypes.string,
    listBgColor: PropTypes.string,
    itemHoverColor: PropTypes.string,
    arrowColor: PropTypes.string,
    defaultItem: PropTypes.any,
    disabled: PropTypes.bool,
    reset: PropTypes.bool
};

Datalist.defaultProps = {
    onClick: () => ({}),
    id: 'id',
    value: 'value',
    children: 'Click me!',
    listBgColor: '#000',
    itemHoverColor: '#555',
    arrowColor: '#f5f5f5',
    listItems: [{ label: 'Menu 1', id: 'menu1' }],
    defaultItem: -1,
    placeholder: '',
    disabled: false,
    reset: false
};

export default Datalist;