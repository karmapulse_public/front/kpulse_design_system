import { css } from 'glamor';

export default ((fontColor, bgColor) => css({
    position: 'relative',
    padding: '2px 0px 2px 28px',
    ' input': {
        position: 'absolute',
        zIndex: 1,
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        margin: 0,
        opacity: 0,
        cursor: 'pointer',
        ':checked + span:after': {
            right: 'calc(100% + 12px)',
            width: 6,
            height: 12,
            borderColor: bgColor,
            borderTopWidth: 0,
            borderLeftWidth: 0,
            borderTopColor: 'transparent',
            borderLeftColor: 'transparent',
            transform: 'translate(0, -70%) rotate(45deg)',
            opacity: 1
        },
        ':disabled': {
            cursor: 'default',
            ' + span': {
                opacity: 0.2
            },
            ' + span:after': {
                opacity: 1
            }
        }
    },
    ' span': {
        position: 'relative',
        zIndex: 0,
        color: fontColor,
        fontSize: '14px',
        fontWeight: 400,
        ':after': {
            content: '""',
            position: 'absolute',
            top: '50%',
            right: 'calc(100% + 5px)',
            width: 18,
            height: 18,
            border: `solid 2px ${fontColor}`,
            opacity: 0.2,
            transform: 'translate(0, -50%) rotate(0deg)',
            transition: 'all 0.25s ease'
        }
    }
}));