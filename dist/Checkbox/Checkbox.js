var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './styles';

const propTypes = {
    value: PropTypes.any,
    label: PropTypes.string,
    fontColor: PropTypes.string,
    bgColor: PropTypes.string,
    onChange: PropTypes.func,
    initChecked: PropTypes.bool,
    disabled: PropTypes.bool,
    reset: PropTypes.bool
};

const defaultProps = {
    value: 0,
    label: '',
    fontColor: '',
    bgColor: '',
    onChange: () => ({}),
    initChecked: false,
    disabled: false,
    reset: false
};

const Checkbox = props => {
    const [check, setCheck] = useState(props.initChecked);
    if (props.reset && check !== props.initChecked) {
        setCheck(props.initChecked);
    }
    return React.createElement(
        'div',
        _extends({}, props, styles(props.fontColor, props.bgColor)),
        React.createElement('input', {
            type: 'checkbox',
            disabled: props.disabled,
            checked: check,
            value: props.value,
            onClick: e => {
                props.onChange(e);setCheck(e.target.checked);
            }
        }),
        React.createElement(
            'span',
            null,
            props.label
        )
    );
};

Checkbox.propTypes = propTypes;
Checkbox.defaultProps = defaultProps;

export default Checkbox;