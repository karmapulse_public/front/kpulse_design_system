var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';

import styles from './styles';

const OrderButton = props => {
    const [order, setOrder] = useState(props.defaultOrder);
    const onClick = () => {
        const newOrder = order === 'UP' ? 'DOWN' : 'UP';
        props.onClick(newOrder);
        setOrder(newOrder);
    };
    const newOrder = props.currentOrder !== '' ? props.currentOrder : order;

    return React.createElement(
        'button',
        _extends({}, styles(props), {
            style: props.style,
            disabled: props.disabled,
            className: 'kp-order-button',
            onClick: props.onChange ? props.onChange : onClick
        }),
        React.createElement(
            'span',
            null,
            props.label
        ),
        React.createElement(
            'div',
            null,
            React.createElement(FontAwesomeIcon, {
                icon: faCaretUp,
                className: newOrder === 'UP' ? 'arrow-selected' : 'arrow'
            }),
            React.createElement(FontAwesomeIcon, {
                icon: faCaretDown,
                className: newOrder === 'DOWN' ? 'arrow-selected' : 'arrow'
            })
        )
    );
};

OrderButton.propTypes = {
    defaultOrder: PropTypes.string,
    currentOrder: PropTypes.string,
    onChange: PropTypes.any,
    onClick: PropTypes.func,
    style: PropTypes.shape({}),
    label: PropTypes.string,
    round: PropTypes.bool,
    ghost: PropTypes.bool,
    large: PropTypes.bool,
    disabled: PropTypes.bool,
    secondary: PropTypes.bool
};

OrderButton.defaultProps = {
    defaultOrder: 'UP',
    currentOrder: '',
    onChange: null,
    onClick: () => ({}),
    style: {},
    label: 'Click me!',
    large: false,
    round: false,
    ghost: false,
    disabled: false,
    secondary: false
};

export default OrderButton;