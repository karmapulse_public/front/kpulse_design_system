var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
import MenuItems from '../MenuItems';

import styles from './styles';

class Dropdown extends Component {
    constructor(props) {
        super(props);
        this.handleOnClick = this.handleOnClick.bind(this);
        this.hideList = this.hideList.bind(this);
        this.hideOnOutClick = this.hideOnOutClick.bind(this);
        const defaultItem = props.listItems.find(i => i[props.id] === props.defaultItem);
        this.me = React.createRef();
        this.state = {
            clicked: false,
            selectedItem: defaultItem ? defaultItem[props.value] : '',
            toolData: ''

        };
    }

    componentDidMount() {
        window.addEventListener('resize', () => this.hideList(false));
        window.addEventListener('click', this.hideOnOutClick);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.listItems !== this.props.listItems || nextProps.reset !== this.props.reset && nextProps.reset) {
            const defaultItem = nextProps.listItems.find(i => i[this.props.id] === nextProps.defaultItem);
            this.setState({
                clicked: false,
                selectedItem: defaultItem ? defaultItem[nextProps.value] : ''
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', () => this.hideList(false));
        window.removeEventListener('click', this.hideOnOutClick);
    }

    hideOnOutClick(e) {
        if (!this.me.current.contains(e.target)) {
            this.hideList(false);
        }
    }

    hideList(setValue = null) {
        this.setState({
            clicked: setValue === null ? !this.state.clicked : setValue
        });
    }

    handleOnClick(item) {
        this.props.onClick(item);
        this.setState({
            selectedItem: item[this.props.value]
        });
    }
    dataTooltip(e) {
        // console.log(e.nativeEvent);
        this.setState({
            toolData: e.nativeEvent.srcElement.innerText
        });
    }

    render() {
        const { clicked, selectedItem, toolData } = this.state;
        const {
            listItems,
            disabled,
            enableTooltip
        } = this.props;
        const visible = clicked === false ? 'closed' : 'opened';
        const disabClass = disabled === false ? '' : 'disabled';
        const tooltip = e => {
            const enable = e;
            return React.createElement(
                'div',
                null,
                enable.length > 30 ? React.createElement(ReactTooltip, { className: 'extraClass', place: 'bottom', type: 'light', effect: 'float' }) : ''
            );
        };

        return React.createElement(
            'div',
            _extends({}, styles(this.props), {
                ref: this.me,
                className: `select select--${visible} select--${disabClass}`
            }),
            React.createElement(
                'button',
                {
                    onClick: () => this.hideList()
                },
                selectedItem
            ),
            React.createElement(
                'ul',
                {
                    onMouseMove: e => this.dataTooltip(e),
                    'data-tip': `${toolData}`
                },
                React.createElement(MenuItems, {
                    value: this.props.value,
                    items: listItems,
                    onChange: this.handleOnClick,
                    onClickItem: this.hideList

                })
            ),
            enableTooltip === true ? tooltip(toolData) : '',
            tooltip(enableTooltip)
        );
    }
}

Dropdown.propTypes = {
    onClick: PropTypes.func,
    listItems: PropTypes.array,
    children: PropTypes.any,
    id: PropTypes.string,
    value: PropTypes.string,
    listBgColor: PropTypes.string,
    itemHoverColor: PropTypes.string,
    arrowColor: PropTypes.string,
    defaultItem: PropTypes.any,
    disabled: PropTypes.bool,
    enableTooltip: PropTypes.bool,
    reset: PropTypes.bool
};

Dropdown.defaultProps = {
    onClick: () => ({}),
    id: 'id',
    value: 'value',
    children: 'Click me!',
    listBgColor: '#000',
    itemHoverColor: '#555',
    arrowColor: '#f5f5f5',
    listItems: [{ label: 'Menu 1', id: 'menu1' }],
    defaultItem: -1,
    disabled: false,
    enableTooltip: false,
    reset: false
};

export default Dropdown;