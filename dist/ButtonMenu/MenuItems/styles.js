import { css } from 'glamor';

export default (props => css({
    background: props.bgColor,
    textAlign: 'center',
    fontSize: 14,
    position: 'absolute',
    minWidth: 180,
    top: '110%',
    left: '-400%',
    zIndex: 99,
    opacity: props.clicked ? 1 : 0,
    pointerEvents: props.clicked ? 'auto' : 'none',
    transition: 'transform 250ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, opacity 250ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
    '&::before': {
        content: '""',
        width: 0,
        height: 0,
        position: 'absolute',
        top: '-8px',
        right: '13px',
        borderLeft: '8px solid transparent',
        borderRight: '8px solid transparent',
        borderBottom: `8px solid ${props.bgColor}`
    },
    ' > button': {
        width: '100%',
        height: 42,
        padding: 15,
        display: 'flex',
        alignItems: 'center',
        background: props.bgColor,
        color: '#fff',
        border: 'none',
        borderTop: '1px solid rgba(255, 255, 255, 0.2)',
        outline: 'none',
        cursor: 'pointer',
        '&:first-child': {
            borderTop: 'none'
        },
        '&:hover': {
            background: props.hoverColor
        }
    }
}));