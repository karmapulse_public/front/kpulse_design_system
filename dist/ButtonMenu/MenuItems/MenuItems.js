import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles';

const propTypes = {
    items: PropTypes.array,
    clicked: PropTypes.bool,
    onClickItem: PropTypes.func
};

const MenuItems = props => React.createElement(
    'div',
    styles(props),
    props.items.map(item => React.createElement(
        'button',
        {
            onClick: () => {
                item.onClick();
                props.onClickItem();
                item.label();
            }
        },
        item.label
    ))
);

MenuItems.propTypes = propTypes;
MenuItems.defaultProps = {
    items: [],
    clicked: false,
    onClickItem: () => ({})
};

export default MenuItems;