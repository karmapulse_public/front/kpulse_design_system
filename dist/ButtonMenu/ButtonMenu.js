var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles';
import MenuItems from './MenuItems';

class ButtonMenu extends Component {
    constructor(props) {
        super(props);
        this.handleOnClick = this.handleOnClick.bind(this);
        this.state = {
            clicked: false
        };
    }

    componentDidMount() {
        window.addEventListener('resize', () => {
            this.setState({ clicked: false });
        });
    }

    componentWillUnmount() {
        window.removeEventListener('resize', () => {
            this.setState({ clicked: false });
        });
    }

    handleOnClick() {
        this.setState({
            clicked: !this.state.clicked
        });
    }

    render() {
        const { clicked } = this.state;
        const {
            children, listBgColor, itemHoverColor, listItems
        } = this.props;
        return React.createElement(
            'div',
            { style: { position: 'relative' } },
            React.createElement(
                'button',
                _extends({}, styles(), {
                    onClick: this.handleOnClick
                }),
                children
            ),
            React.createElement(MenuItems, {
                clicked: clicked,
                items: listItems,
                bgColor: listBgColor,
                hoverColor: itemHoverColor,
                onClickItem: this.handleOnClick
            })
        );
    }
}

ButtonMenu.propTypes = {
    listItems: PropTypes.array,
    children: PropTypes.element,
    listBgColor: PropTypes.string,
    itemHoverColor: PropTypes.string
};
ButtonMenu.defaultProps = {
    children: 'Click me!',
    listBgColor: '#000',
    itemHoverColor: '#555',
    listItems: [{ label: 'Menu 1', onClick: () => ({}) }]
};

export default ButtonMenu;