import { css } from 'glamor';

export default (() => css({
    height: 32,
    fontSize: 14,
    padding: '0px 15px',
    background: 'none',
    border: 'none',
    outline: 'none',
    boxSizing: 'border-box',
    display: 'flex',
    alignItems: 'center',
    opacity: 1,
    cursor: 'pointer',
    '&:hover': {
        opacity: 0.7
    }
}));