import React from 'react';
import PropTypes from 'prop-types';

import styles from './ToggleStyles';

const Toggle = props => React.createElement(
    'div',
    styles(props),
    React.createElement(
        'div',
        { className: 'container' },
        React.createElement(
            'label',
            { className: 'switch', htmlFor: 'checkbox' },
            React.createElement('input', { type: 'checkbox', id: 'checkbox', onClick: props.onClick }),
            React.createElement('div', { className: 'slider round' })
        )
    ),
    React.createElement(
        'div',
        { className: 'text' },
        props.children
    )
);
Toggle.propTypes = {
    onClick: PropTypes.func,
    children: PropTypes.any
};

Toggle.defaultProps = {
    onClick: () => ({}),
    children: null
};

export default Toggle;